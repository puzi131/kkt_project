﻿//#define UNITY_ANDROID

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    public TextMeshProUGUI text;
    public const int inputIndex = 0;
    private Vector2 mouseButtonDownPos;
    private Vector2 diffPosVect;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        InputUpdate();
    }

    private void FixedUpdate()
    {
        PlayerMove();
    }
#if DEBUG
    private void InputUpdate()
    {
        if (Input.GetMouseButton(inputIndex))
        {
            if (Input.GetMouseButtonDown(inputIndex))
            {
                mouseButtonDownPos = Input.mousePosition;

            }
            diffPosVect = (Vector2)Input.mousePosition - mouseButtonDownPos;
        } else
        {
            diffPosVect = Vector2.zero;
        }
        text.text = diffPosVect.ToString();
    }
#else
    private void InputUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            string tempString = "";
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    tempString = "TouchPhase Began!";

                    break;

                case TouchPhase.Moved:
                    tempString = "TouchPhase Moved!";

                    break;

                case TouchPhase.Stationary:
                    tempString = "TouchPhase Stationary!";

                    break;

                case TouchPhase.Ended:
                    tempString = "TouchPhase Ended!";

                    break;

                case TouchPhase.Canceled:
                    tempString = "TouchPhase Canceled!";

                    break;
            }
            text.text = tempString;
        }
    }
#endif

    private void PlayerMove()
    {
        Vector2 calDiffVect = Vector2.ClampMagnitude(diffPosVect / 100, 3f);
        transform.Translate(calDiffVect * Time.deltaTime);
    }
}
