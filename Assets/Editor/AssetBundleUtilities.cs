﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

[System.Serializable]
public class AssetInfo
{
	public string assetName;
	public string hash128;
}

[System.Serializable]
public class AssetBundleInfo
{
	public AssetInfo[] asset;
}

	/// <summary>
	/// AssetBundle作成関連処理
	/// </summary>
	public class AssetBundleUtilities
{
	/// <summary>
	/// プラットフォームの取得
	/// </summary>
	/// <returns></returns>
	public static string GetPlatform()
    {
        switch (EditorUserBuildSettings.activeBuildTarget)
        {
            case BuildTarget.Android:
                return "Android";
            case BuildTarget.iOS:
                return "iOS";
            case BuildTarget.WebGL:
                return "WebGL";
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                return "Windows";
            case BuildTarget.Switch:
                return "Switch";
            case BuildTarget.PS4:
                return "PS4";
            //case BuildTarget.XBOX360:
            case BuildTarget.XboxOne:
                return "Xbox";
#if UNITY_2017
            case BuildTarget.StandaloneOSX:
                return "OSX";
#endif
			default:
                return "";
        }
    }


    /// <summary>
    /// アセットバンドル置き場のパス
    /// </summary>
    public static string AssetbundlePath
    {
        //get { return "Assets" + Path.DirectorySeparatorChar + "assetbundles" + Path.DirectorySeparatorChar; }
        get { return "AssetBundles/" + GetPlatform(); }
    }


    /// <summary>
    /// ストリーミングアセットバンドル置き場のパス
    /// </summary>
    public static string StreamingAssetPath
    {
        get { return "Assets/StreamingAssets/" + GetPlatform(); }
    }


	/// <summary>
	/// アセットバンドルチェックファイルのパス
	/// </summary>
	public static string CheckFilePath {
		get { return "AssetBundles/" + GetPlatform() + "/CheckFile.json"; }
	}

	/// <summary>
	/// AssetBundleのビルド
	/// </summary>
	[MenuItem("Assets/Build AssetBundles")]
	public static void BuildAssetBundles()
	{
		string platform = GetPlatform();

		if (platform == "") {
			Debug.LogError("プラットフォームの取得に失敗");
			return;
		}


		if (!Directory.Exists(AssetbundlePath))
			Directory.CreateDirectory(AssetbundlePath);

		// AssetBundleのビルド
		string[] ABNames = AssetDatabase.GetAllAssetBundleNames();
		AssetBundleBuild[] buildMap = new AssetBundleBuild[ABNames.Length];
		Debug.Log("Number of building asset bundles : " + buildMap.Length);
		System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
		sw.Start();
		for (int i = 0; i < buildMap.Length; i++) {
			buildMap[i].assetBundleName = ABNames[i] + ".assetbundle";
			buildMap[i].assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(ABNames[i]);
			//Debug.Log("ABName : " + buildMap[i].assetBundleName);
			//for (int j=0;j<buildMap[i].assetNames.Length;j++)
			//{
			//    Debug.Log("buildMap : "+buildMap[i].assetNames[j]);
			//}
		}
		BuildPipeline.BuildAssetBundles(AssetbundlePath, buildMap, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
		AssetDatabase.Refresh();
		CreateStreamingAssets();

		sw.Stop();
		Debug.Log("build time : " + sw.ElapsedMilliseconds.ToString() + "ms");
	}

	/// <summary>
	/// Master DataのAssetBundleのビルド
	/// </summary>
	[MenuItem("Assets/Build AssetBundles of Master Data")]
    public static void BuildAssetBundlesOfMasterData()
    {
        string platform = GetPlatform();

        if (platform == "")
        {
            Debug.LogError("プラットフォームの取得に失敗");
            return;
        }


        if (!Directory.Exists(AssetbundlePath))
            Directory.CreateDirectory(AssetbundlePath);

        // AssetBundleのビルド
        var ABNames = Directory.GetFiles("Assets/AssetBundles/Windows/data", "*.assetbundle");
        for (int i = 0; i < ABNames.Length; i++)
        {
            ABNames[i] = ABNames[i].Replace("Assets/AssetBundles/Windows/", "");
            ABNames[i] = ABNames[i].Replace(".assetbundle", "");
            ABNames[i] = ABNames[i].Replace("\\", "/");
        }
        AssetBundleBuild[] buildMap = new AssetBundleBuild[ABNames.Length];
        Debug.Log("Number of building asset bundles of master datas: " + buildMap.Length);

        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        for (int i = 0; i < buildMap.Length; i++)
        {
            buildMap[i].assetBundleName = ABNames[i] + ".assetbundle";
            buildMap[i].assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(ABNames[i]);
            //Debug.Log("ABName : " + buildMap[i].assetBundleName);
            //for (int j=0;j<buildMap[i].assetNames.Length;j++)
            //{
            //    Debug.Log("buildMap : "+buildMap[i].assetNames[j]);
            //}
        }

        BuildPipeline.BuildAssetBundles(AssetbundlePath, buildMap, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
        AssetDatabase.Refresh();
        CreateStreamingAssets();

        sw.Stop();
        Debug.Log("build time : " + sw.ElapsedMilliseconds.ToString() + "ms");
    }

	/// <summary>
	/// AssetBundleのビルド
	/// </summary>
	[MenuItem("Assets/Update AssetBundles")]
	public static void UpdateAssetBundles()
	{
		string platform = GetPlatform();

		if (platform == "") {
			Debug.LogError("プラットフォームの取得に失敗");
			return;
		}

		if (!Directory.Exists(AssetbundlePath))
			Directory.CreateDirectory(AssetbundlePath);

		// アセットデータベース内の未使用のアセットバンドル名をすべて削除します。 時間がかかります
		//AssetDatabase.RemoveUnusedAssetBundleNames();

		// AssetBundleのビルド
		string[] ABNames = AssetDatabase.GetAllAssetBundleNames();
		AssetBundleBuild[] buildMap = new AssetBundleBuild[ABNames.Length];
		Debug.Log("Number of building asset bundles : " + buildMap.Length);

		System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
		sw.Start();

		Dictionary<string, Hash128> assetDic = new Dictionary<string, Hash128>();

		AssetBundleInfo oldInfo = new AssetBundleInfo();
		if (File.Exists(CheckFilePath)) {
			string oldJson = File.ReadAllText(CheckFilePath);
			if (!string.IsNullOrEmpty(oldJson)) {

				//Debug.Log("Json : " + oldJson);

				oldInfo = JsonUtility.FromJson<AssetBundleInfo>(oldJson);
			}
		}
		// ファイルに情報があれば
		if (oldInfo.asset != null && oldInfo.asset.Length > 0) {

			Debug.Log("assetDic : " + oldInfo.asset.Length);

			for (int i = 0; i < oldInfo.asset.Length; i++) {
				assetDic.Add(oldInfo.asset[i].assetName, Hash128.Parse(oldInfo.asset[i].hash128));
			}

		} else {

			// 重複ファイルを除く
			for (int i = 0; i < buildMap.Length; i++) {
				string[] vs = AssetDatabase.GetAssetPathsFromAssetBundle(ABNames[i]);
				string[] allVS = AssetDatabase.GetDependencies(vs, true);
				//string[] vs = AssetDatabase.GetAssetBundleDependencies(ABNames[i], true);
				for (int j = 0; j < allVS.Length; j++) {

					//Debug.Log("asset name (0) : " + vs[j]);

					if (!assetDic.ContainsKey(allVS[j])) {
						assetDic.Add(allVS[j], AssetDatabase.GetAssetDependencyHash(allVS[j]));
					}
				}
			}
		}

		Debug.Log("Number of asset num : " + assetDic.Count);

		List<AssetBundleBuild> buildList = new List<AssetBundleBuild>();
		Dictionary<string, string> newDic = new Dictionary<string, string>();

		for (int i = 0; i < buildMap.Length; i++) {
			bool update = false;
			string[] vs = AssetDatabase.GetAssetPathsFromAssetBundle(ABNames[i]);
			string[] allVS = AssetDatabase.GetDependencies(vs, true);

			for (int j = 0; j < allVS.Length; j++) {
				// 新しいファイルかファイルが更新されていたら更新対象にする
				if (update == false && (!assetDic.ContainsKey(allVS[j]) || assetDic[allVS[j]].Equals(AssetDatabase.GetAssetDependencyHash(allVS[j])) == false)) {
					//Debug.Log("Update : " + vs[j]);
					update = true;
				}

				// チェックファイル更新のための情報作成
				if (!newDic.ContainsKey(allVS[j])) {
					//Debug.Log("asset name : " + vs[j] + " Hash : " + AssetDatabase.GetAssetDependencyHash(vs[j]).ToString());
					newDic.Add(allVS[j], AssetDatabase.GetAssetDependencyHash(allVS[j]).ToString());
				}
				//Debug.Log("ABName : " + buildMap[i].assetBundleName);
				//for (int j=0;j<buildMap[i].assetNames.Length;j++)
				//{
				//    Debug.Log("buildMap : "+buildMap[i].assetNames[j]);
				//}
			}

			if (update) {
				AssetBundleBuild build = new AssetBundleBuild();

				build.assetBundleName = ABNames[i] + ".assetbundle";
				build.assetNames = vs;
				//build.assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(ABNames[i]);

				buildList.Add(build);

				Debug.Log("Update asset bundles : " + build.assetBundleName);
			}
		}

		Debug.Log("Number of Update asset bundles : " + buildList.Count);

		if (buildList.Count > 0) {
			AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles(AssetbundlePath, buildList.ToArray(), BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
			//BuildPipeline.BuildAssetBundles(AssetbundlePath, buildMap, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
			if (manifest == null) {
				Debug.LogError("上手くビルドできませんでした orz");
			} else {
				Debug.Log("ビルド終了です");
			}
		}
		AssetDatabase.Refresh();
		CreateStreamingAssets();

		AssetBundleInfo newInfo = new AssetBundleInfo();
		newInfo.asset = new AssetInfo[1];
		if (newDic.Count > 0) {
			Array.Resize<AssetInfo>(ref newInfo.asset, newDic.Count);
			int index = 0;
			foreach (KeyValuePair<string, string> item in newDic) {

				//Debug.Log("newDic asset : " + item.Key + " Hash : " + item.Value);
				AssetInfo info = new AssetInfo();
				info.assetName = item.Key;
				info.hash128 = item.Value;

				newInfo.asset[index] = info;
				index++;
			}
			string json = JsonUtility.ToJson(newInfo);
			File.WriteAllText(CheckFilePath, json);
		}

		sw.Stop();
		Debug.Log("build time : " + sw.ElapsedMilliseconds.ToString() + "ms");
	}

	/// <summary>
	/// AssetBundleのビルド
	/// </summary>
	[MenuItem("Assets/Update AssetBundles Select (Force)")]
	public static void UpdateAssetBundlesSelect()
	{
		string platform = GetPlatform();

		if (platform == "") {
			Debug.LogError("プラットフォームの取得に失敗");
			return;
		}

		if (!Directory.Exists(AssetbundlePath))
			Directory.CreateDirectory(AssetbundlePath);

		System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
		sw.Start();

		// Get all selected *assets*
		if (Selection.objects.Length < 1) {
			Debug.LogError("ファイルが選択されていません");
			return;
		}

		// AssetBundleのビルド
		List<AssetBundleBuild> assetBundleBuilds = new List<AssetBundleBuild>();
		HashSet<string> processedBundles = new HashSet<string>();

		// Get asset bundle names from selection
		foreach (UnityEngine.Object o in Selection.objects) {
			var bundlePath = AssetDatabase.GetAssetPath(o);

			Debug.Log(bundlePath);

			// bundle 名だけにする
			int index = bundlePath.LastIndexOf("/");
			var assetBundleName = bundlePath.Substring(index + 1, bundlePath.LastIndexOf(".assetbundle") - 1 - index); // Variantがあるかもしれないので
			var assetBundleFullName = assetBundleName;

			/*
			var importer = AssetImporter.GetAtPath(assetPath);

			if (importer == null) {
				continue;
			}

			// Get asset bundle name & variant
			var assetBundleName = importer.assetBundleName;
			var assetBundleVariant = importer.assetBundleVariant;
			var assetBundleFullName = string.IsNullOrEmpty(assetBundleVariant) ? assetBundleName : assetBundleName + "." + assetBundleVariant;
			*/

			// Only process assetBundleFullName once. No need to add it again.
			if (processedBundles.Contains(assetBundleFullName)) {
				continue;
			}

			processedBundles.Add(assetBundleFullName);

			AssetBundleBuild build = new AssetBundleBuild();

			build.assetBundleName = assetBundleName + ".assetbundle";
			//build.assetBundleVariant = assetBundleVariant;
			//build.assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(bundlePath);
			string[] ABNames = AssetDatabase.GetAllAssetBundleNames();
			string target = "";
			for (int k=0; k<ABNames.Length; k++) {
				index = ABNames[k].LastIndexOf("/");
				if (ABNames[k].Substring(index + 1).Equals(assetBundleFullName)) {
					target = ABNames[k];
					break;
				}
			}
			build.assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(target);


			if (build.assetBundleName.Length == 0) {
				continue;
			}
			assetBundleBuilds.Add(build);

			Debug.Log("ビルド : " + AssetbundlePath + "/" + build.assetBundleName);
		}

		if (assetBundleBuilds.Count == 0) {
			Debug.LogError("有効なファイルが選択されていません");
			return;
		}

		AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles(AssetbundlePath, assetBundleBuilds.ToArray(), BuildAssetBundleOptions.ChunkBasedCompression | BuildAssetBundleOptions.ForceRebuildAssetBundle, EditorUserBuildSettings.activeBuildTarget);
		if (manifest == null) {
			Debug.LogError("上手くビルドできませんでした orz");
		} else {
			Debug.Log("ビルド終了です");
		}
		AssetDatabase.Refresh();
		CreateStreamingAssets();

		sw.Stop();
		Debug.Log("build time : " + sw.ElapsedMilliseconds.ToString() + "ms");
	}

	private static void CreateStreamingAssets()
    {
        if (!Directory.Exists(StreamingAssetPath))
            Directory.CreateDirectory(StreamingAssetPath);

		Debug.Log("Copy : " + AssetbundlePath + " >> " + StreamingAssetPath);

		/*
		if (AssetDatabase.CopyAsset(AssetbundlePath, StreamingAssetPath) == false) {
			Debug.LogError("ファイルのコピーに失敗しました");
		}
		*/
        DirectoryInfo dir = new DirectoryInfo(AssetbundlePath);
        FileInfo[] info = dir.GetFiles("*.assetbundle", SearchOption.AllDirectories);

        for (int i = 0; i < info.Length; i++)
        {
			// 更新されたファイルのみコピーする
			if (System.IO.File.GetLastWriteTime(info[i].FullName) > System.IO.File.GetLastWriteTime(StreamingAssetPath + "/" + info[i].Name)) {
				try {
					File.Copy(info[i].FullName, StreamingAssetPath + "/" + info[i].Name, true);
				}
				catch (IOException e) {
					Debug.LogError("ファイルのコピーに失敗 : " + info[i].FullName + "\nError : " + e);
				}
			}
        }
        AssetDatabase.Refresh();
		
	}


    /// <summary>
    /// AssetBundle指定されているファイルの削除
    /// ※滅多なことが無い限り使用しないと思うのでコメントアウト
    /// </summary>
    //[MenuItem("Assets/Delete AssetBundles Resources")]
    //public static void DeleteAssetBundleResources()
    //{
    //    int success = 0;
    //    int failed = 0;

    //    // 全アセットバンドル名を取得
    //    string[] assetBundleNames = AssetDatabase.GetAllAssetBundleNames();
    //    foreach (string abName in assetBundleNames)
    //    {
    //        // アセットバンドル名が付いている全ファイルのパスを取得
    //        string[] assetBundlePath = AssetDatabase.GetAssetPathsFromAssetBundle(abName);

    //        foreach (string path in assetBundlePath)
    //        {
    //            try
    //            {
    //                FileInfo fileInfo = new FileInfo(path);
    //                if (fileInfo.Exists)
    //                    fileInfo.Delete();
    //                Debug.Log("DeleteAssetBundleResources : " + path + "の削除に成功");

    //                fileInfo = new FileInfo(path + ".meta");
    //                if (fileInfo.Exists)
    //                    fileInfo.Delete();
    //                Debug.Log("DeleteAssetBundleResources : " + path + ".meta" + "の削除に成功");
    //                success++;
    //            }
    //            catch
    //            {
    //                Debug.LogError("DeleteAssetBundleResources : " + path + "の削除に失敗");
    //                failed++;
    //            }
    //        }
    //    }

    //    Debug.Log("DeleteAssetBundleResources : Done. (削除したファイル数：" + success + "／失敗したファイル数：" + failed + ")");

    //    // 成功した場合はアセットを更新
    //    if (success > 0)
    //        AssetDatabase.Refresh(ImportAssetOptions.Default);
    //}
}