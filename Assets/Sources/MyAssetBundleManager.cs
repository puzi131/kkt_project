﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


/// <summary>
/// AssetBundleロード関連処理
/// TODO: delegate処理関連があまりよろしい設計ではないので、時間があれば直す
/// </summary>
public class MyAssetBundleManager : MonoBehaviour
{
    //--------------------------------------------------------------------
    // public static variable
    //--------------------------------------------------------------------
    public static WaitForSeconds m_loadWaitTime = new WaitForSeconds(0.1f);                 //ロード中のロード完了待ち時間
    public static List<DelegateAfterLoadAB> afterDoList = new List<DelegateAfterLoadAB>();  //ロード完了後の処理待ちリスト
    public static List<int> delegateIdList = new List<int>();                               //ロード完了後の処理待ちリストの各引数


    //--------------------------------------------------------------------
    // private static variable
    //--------------------------------------------------------------------
    private static Dictionary<string, ABLoadReqData> m_ABReqload = new Dictionary<string, ABLoadReqData>(); //読み込んだバンドルのリクエストデータ群

    //--------------------------------------------------------------------
    // public delegate variable
    //--------------------------------------------------------------------
    public delegate void DelegateAfterLoadAB(int tmpNum);   //ロード完了後の処理

    //--------------------------------------------------------------------
    // Request Data
    //--------------------------------------------------------------------
    /// <summary>
    /// AssetBundleリクエストデータ
    /// </summary>
    public class ABLoadReqData
    {
        // リクエストに使用するデータ
        public string m_bundleName;                         // バンドル名
        public string m_assetName;                          // アセット名
        public Type m_bundleType;                           // バンドルのデータ型
        public DelegateAfterLoadAB m_afterLoadAB;           // AssetBundle読み込み後に行う処理
        public int m_delegateNum;                           // AssetBundle読み込み後に行う処理用の引数

        // リクエスト後にデータが入るデータ
        public ABLoadReq m_bundleLoadReq;                   // アセットバンドル読み込み情報
        public object[] m_assetDatas;                       // 読み込んだアセットのデータ群
        public object m_assetData;                          // 指定したアセット名のアセット


        /// <summary>
        /// AssetBundle読み込みデータのコンストラクタ
        /// </summary>
        /// <param name="bundleName">バンドル名</param>
        /// <param name="type">バンドルのデータ型</param>
        /// <param name="afterLoadAB">このバンドル読み込みが終了後の処理</param>
        /// <param name="assetName">アセット名</param>
        public ABLoadReqData(string bundleName, Type type, DelegateAfterLoadAB afterLoadAB, int delegateNum = 0, string assetName = "")
        {
            m_bundleName = bundleName;
            m_bundleType = type;
            m_afterLoadAB = afterLoadAB;
            m_delegateNum = delegateNum;
            m_bundleLoadReq = new ABLoadReq();
            m_assetDatas = null;
            m_assetName = assetName;
            m_assetData = null;
        }
    }


    /// <summary>
    /// AssetBundleリクエスト情報
    /// </summary>
    public class ABLoadReq
    {
        public enum ABLoadState
        {
            NoLoad = 0,
            ABRequested,
            LoadEnd,
            LoadErrorEnd,
        }

        public ABLoadState m_loadState;                 // ロード状況
        public AssetBundleCreateRequest m_ABCreateReq;  // アセットバンドルのリクエストデータ(同期処理時 null)
        public AssetBundle m_assetBundle;               // アセットバンドルデータ(同期処理の場合はこちらのみ)

        /// <summary>
        /// AssetBundleリクエスト情報のコンストラクタ
        /// </summary>
        public ABLoadReq()
        {
            m_ABCreateReq = null;
            m_assetBundle = null;
            m_loadState = ABLoadState.NoLoad;
        }
    }

    //--------------------------------------------------------------------
    // Return of Something info
    //--------------------------------------------------------------------
    /// <summary>
    /// プラットフォームの取得
    /// </summary>
    /// <returns>プラットフォーム名</returns>
    public static string GetPlatform()
    {
        string platform = "";

        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                platform = "Android";
                break;
            case RuntimePlatform.IPhonePlayer:
                platform = "iOS";
                break;
            case RuntimePlatform.WebGLPlayer:
                platform = "WebGL";
                break;
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.WSAPlayerARM:
            case RuntimePlatform.WSAPlayerX86:
            case RuntimePlatform.WSAPlayerX64:
                platform = "Windows";
                break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
                platform = "OSX";
                break;
            case RuntimePlatform.PS4:
                platform = "PS4";
                break;
            case RuntimePlatform.XboxOne:
                platform = "Xbox";
                break;
            //case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.Switch:
                platform = "Switch";
                break;
            default:
                break;
        }

        if (platform == "")
            return platform;
        else
            return platform + "/";
    }


    /// <summary>
    /// アセットバンドル置き場のパス
    /// </summary>
    public static string AssetbundlePath
    {
        get {
            return Application.streamingAssetsPath + "/" + GetPlatform();
        }
    }


    /// <summary>
    /// AssetBundleのロードデータの取得
    /// </summary>
    /// <param name="bundleName">バンドル名</param>
    /// <returns>AssetBundleのロードデータ</returns>
    private static ABLoadReqData GetABLoadReq(string bundleName)
    {
        if (m_ABReqload == null || m_ABReqload.Count <= 0 || bundleName == "")
            return null;

        if (m_ABReqload.ContainsKey(bundleName))
            return m_ABReqload[bundleName];
        else
            return null;
    }



    //--------------------------------------------------------------------
    // Load AssetBundle Process
    //--------------------------------------------------------------------
    /// <summary>
    /// AssetBundleのリクエスト処理
    /// </summary>
    /// <param name="mono">リクエストを行うMonoBehaviour</param>
    /// <param name="abLoadData">AssetBundleのロードデータ</param>
    /// <returns>AssetBundleのロードデータ</returns>
    public static ABLoadReqData RequestAssetBundle(MonoBehaviour mono, ABLoadReqData abLoadData)
    {
        if (abLoadData == null)
            return null;

        mono.StartCoroutine(ProcessReqAB(abLoadData));
        return abLoadData;
    }

    /// <summary>
    /// AssetBundleのリクエスト処理
    /// </summary>
    /// <param name="mono">リクエストを行うMonoBehaviour</param>
    /// <param name="abLoadData">AssetBundleのロードデータ</param>
    /// <param name="afterLoadAB">データ群のリクエスト終了後に行う処理</param>
    /// <param name="delegateNum">データ群のリクエスト終了後に行う処理の引数</param>
    /// <returns>AssetBundleのロードデータ</returns>
    public static ABLoadReqData[] RequestAssetBundle(MonoBehaviour mono, ABLoadReqData[] abLoadData, DelegateAfterLoadAB afterLoadAB = null, int delegateNum = 0)
    {
        if (abLoadData == null)
            return null;

        if (afterLoadAB != null)
        {
            delegateIdList.Add(delegateNum);
            afterDoList.Add(afterLoadAB);
        }

        for(int i=(abLoadData.Length-1);i>=0;i--)
        {
            ABLoadReqData tmpReqData = abLoadData[i];

            mono.StartCoroutine(ProcessReqAB(tmpReqData, i));
        }
        return abLoadData;
    }


    /// <summary>
    /// AssetBundleのリクエスト処理過程
    /// </summary>
    /// <param name="abLoadData">AssetBundleのロードデータ</param>
    private static IEnumerator ProcessReqAB(ABLoadReqData abLoadData, int loadNum = -1)
    {
        if (abLoadData == null || abLoadData.m_bundleName == "")
            yield break;

        ABLoadReqData getalrReqData = GetABLoadReq(abLoadData.m_bundleName);
        if (getalrReqData != null)
        {
            // 既にリクエスト済みの登録があればデータを確認し、まだアセットデータ群がない場合は現在リクエスト中か等のチェックコルーチンへ
            if (getalrReqData.m_assetDatas == null)
                yield return WaitAlreadyLoadAB(abLoadData, loadNum);
            else
                abLoadData.m_assetDatas = getalrReqData.m_assetDatas;

            // 正常にアセットデータ群があった場合は、アセット名指定があった場合のみアセットデータの抽出(一度読み込んだものはすぐ取れるはずなので同期で取得)
            if (abLoadData.m_assetName != "")
                GetAsset(abLoadData.m_bundleLoadReq.m_assetBundle, abLoadData);
        }
        else
        {
            abLoadData.m_bundleLoadReq = ReqAB(abLoadData.m_bundleName, abLoadData.m_bundleLoadReq);

            m_ABReqload.Add(abLoadData.m_bundleName, abLoadData);
            yield return GetAssetBundle(abLoadData, loadNum);
        }

        // 後で行う処理が登録されていた場合は処理
        if (abLoadData.m_afterLoadAB != null)
            abLoadData.m_afterLoadAB(abLoadData.m_delegateNum);

        // 複数読み込みでかつ最後の読み込みデータかどうか
        if (loadNum == 0)
        {
            // 最後の読み込みデータだった場合は、読み込み待ちとなっていた後処理を全て行う
            for(int i=0;i<afterDoList.Count;i++)
            {
                afterDoList[i](delegateIdList[i]);
            }
            afterDoList.RemoveAll(tmp => tmp.GetType() == typeof(DelegateAfterLoadAB));
        }
    }


    /// <summary>
    /// AssetBundleロード処理のリクエスト
    /// </summary>
    /// <param name="bundleName">バンドル名</param>
    /// <param name="lreq">AssetBundleリクエスト情報</param>
    /// <returns>AssetBundleリクエスト情報</returns>
    private static ABLoadReq ReqAB(string bundleName, ABLoadReq lreq)
	{
        try
        {
            if (lreq.m_loadState == ABLoadReq.ABLoadState.LoadEnd && lreq.m_ABCreateReq != null && lreq.m_ABCreateReq.assetBundle != null)
            {
                //既に読み込み済みのものがあるようであれば　破棄してから読み込みするようにします。
                lreq.m_ABCreateReq.assetBundle.Unload(false);
                m_ABReqload.Remove(bundleName);
            }

            bundleName = bundleName.ToLower();
            string bundlePath = Path.Combine(AssetbundlePath, bundleName + ".assetbundle");

#if !UNITY_SWITCH
            lreq.m_ABCreateReq = AssetBundle.LoadFromFileAsync(AssetbundlePath + bundleName + ".assetbundle");
#else

            byte[] data = File.ReadAllBytes(bundlePath);
            if (data == null)
                Debug.LogError(bundleName);
            lreq.m_ABCreateReq = AssetBundle.LoadFromMemoryAsync(data);
#endif

            lreq.m_loadState = ABLoadReq.ABLoadState.ABRequested;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }

		return lreq;    //リクエスト受理
	}

    public static object SyncLoadAB(ABLoadReqData abLoadData)
    {
        if (m_ABReqload.ContainsKey(abLoadData.m_bundleName))
        {
            // 引数のABLoadReqDataも利用する事があるかもしれないので一部データを入れておく
            abLoadData.m_bundleLoadReq.m_assetBundle = m_ABReqload[abLoadData.m_bundleName].m_bundleLoadReq.m_assetBundle;
            abLoadData.m_assetDatas = m_ABReqload[abLoadData.m_bundleName].m_assetDatas;

            return m_ABReqload[abLoadData.m_bundleName].m_assetDatas[0];
        }
        else
        {
#if !UNITY_SWITCH
            abLoadData.m_bundleLoadReq.m_assetBundle = AssetBundle.LoadFromFile(AssetbundlePath + abLoadData.m_bundleName + ".assetbundle");
#else
            abLoadData.m_bundleLoadReq.m_assetBundle = AssetBundle.LoadFromFile(AssetbundlePath + abLoadData.m_bundleName.ToLower() + ".assetbundle");
            //string bundleName = abLoadData.m_bundleName;
            //bundleName = bundleName.ToLower();
            //string bundlePath = Path.Combine(AssetbundlePath, bundleName + ".assetbundle");

            //byte[] data = File.ReadAllBytes(bundlePath);
            //if (data == null)
            //    Debug.LogError(bundleName);

            //abLoadData.m_bundleLoadReq.m_assetBundle = AssetBundle.LoadFromMemory(data);
#endif

            abLoadData.m_assetDatas = abLoadData.m_bundleLoadReq.m_assetBundle.LoadAllAssets();
            
            m_ABReqload.Add(abLoadData.m_bundleName, abLoadData);
            return abLoadData.m_assetDatas[0];
        }
    }

    /// <summary>
    /// AssetBundleデータの取得処理
    /// </summary>
    /// <param name="loadReqData">AssetBundleのロードデータ</param>
    /// <param name="loadNum">残りのロード数(ロードデータ群の引数)</param>
    /// <returns></returns>
    private static IEnumerator GetAssetBundle(ABLoadReqData loadReqData, int loadNum)
    {
        // いきなり読み込める事は無いので少し待つ
        yield return m_loadWaitTime;

        yield return new WaitWhile(() => loadReqData.m_bundleLoadReq.m_ABCreateReq.isDone == false);

        while (loadReqData.m_bundleLoadReq.m_loadState != ABLoadReq.ABLoadState.LoadEnd &&
               loadReqData.m_bundleLoadReq.m_loadState != ABLoadReq.ABLoadState.LoadErrorEnd)
        {
            WaitLoadAB(loadReqData.m_bundleLoadReq, loadReqData.m_bundleName);
            yield return m_loadWaitTime;
        }

        yield return GetAsset(loadReqData);

        if (loadReqData.m_assetDatas == null)
        {
            // LoadAssetに時間がかかっている可能性もあるので一度だけ少し待って再度確認(LoadErrorEnd時はそのままnullで終了)
            yield return m_loadWaitTime;

            if (loadReqData.m_assetDatas == null)
                yield break;
        }

        if (m_ABReqload.ContainsKey(loadReqData.m_bundleName))
            m_ABReqload[loadReqData.m_bundleName].m_assetDatas = loadReqData.m_assetDatas;
        else
            m_ABReqload.Add(loadReqData.m_bundleName, loadReqData);
    }


    /// <summary>
    /// AssetBundleデータの取得完了までの待機処理
    /// </summary>
    /// <param name="loadReqData">AssetBundleのロードデータ</param>
    /// <returns></returns>
    private static IEnumerator WaitAlreadyLoadAB(ABLoadReqData loadReqData, int loadNum = -1)
    {
        // 既に読み込み終わっていたとしてもdelegate処理が変わっている可能性が大きいので
        // 一度は必ず処理させる
        do
        {
            yield return m_loadWaitTime;

            ABLoadReqData tmpLoadReq = GetABLoadReq(loadReqData.m_bundleName);
            if (tmpLoadReq == null)
                continue;

            tmpLoadReq.m_assetName = loadReqData.m_assetName;
            tmpLoadReq.m_afterLoadAB = loadReqData.m_afterLoadAB;
            tmpLoadReq.m_delegateNum = loadReqData.m_delegateNum;

            if (tmpLoadReq.m_assetDatas == null || 
                tmpLoadReq.m_bundleLoadReq.m_loadState <= ABLoadReq.ABLoadState.ABRequested ||
                loadReqData.m_bundleLoadReq.m_loadState == ABLoadReq.ABLoadState.LoadEnd &&
                loadReqData.m_bundleLoadReq.m_ABCreateReq.assetBundle != null)
            {
                continue;
            }
            else if (tmpLoadReq.m_assetDatas == null &&
                (tmpLoadReq.m_bundleLoadReq.m_loadState == ABLoadReq.ABLoadState.LoadEnd || 
                tmpLoadReq.m_bundleLoadReq.m_loadState == ABLoadReq.ABLoadState.LoadErrorEnd))
            {
                //もしGC等で消えていた場合には再度リクエストを行う
                m_ABReqload[loadReqData.m_bundleName] = loadReqData;
                loadReqData.m_bundleLoadReq = ReqAB(loadReqData.m_bundleName, loadReqData.m_bundleLoadReq);
                yield return GetAssetBundle(loadReqData, loadNum);

                yield break;
            }

            if (loadReqData.m_assetName != "")
                yield return GetAssetAsync(tmpLoadReq.m_bundleLoadReq.m_ABCreateReq.assetBundle, loadReqData);

            loadReqData.m_bundleLoadReq.m_assetBundle = tmpLoadReq.m_bundleLoadReq.m_ABCreateReq.assetBundle;
            loadReqData.m_assetDatas = tmpLoadReq.m_assetDatas;
        } while (loadReqData.m_assetDatas == null);
    }


    /// <summary>
    /// AssetBundleのロード待機処理
    /// </summary>
    /// <param name="loadReq">AssetBundleリクエスト情報</param>
    /// <returns>true:ロード終了 false:ロード待ち</returns>
    public static bool WaitLoadAB(ABLoadReq loadReq, string bundleName)
    {
        if (!loadReq.m_ABCreateReq.isDone)
        {
            return false;
        }

        //読み込み完了したけどもデータがちゃんと存在しているのか？
        if (loadReq.m_ABCreateReq.assetBundle != null)
        {
            loadReq.m_loadState = ABLoadReq.ABLoadState.LoadEnd;
            loadReq.m_assetBundle = loadReq.m_ABCreateReq.assetBundle;
        }
        else
        {
            Debug.LogError("assetBundle is Null...");  //ERROR
            loadReq.m_loadState = ABLoadReq.ABLoadState.LoadErrorEnd;
        }

        return true;
    }


    /// <summary>
    /// バンドルデータ群/指定したアセットデータの取得処理
    /// </summary>
    /// <param name="loadReqData">AssetBundleのロードデータ</param>
    private static IEnumerator GetAsset(ABLoadReqData loadReqData)
    {
        // 初期化済みかどうかやロードが終了しているか等をチェックし、まだ出来ていない場合は既定値を返す
        if (loadReqData == null || !loadReqData.m_bundleLoadReq.m_ABCreateReq.isDone || loadReqData.m_bundleLoadReq.m_loadState != ABLoadReq.ABLoadState.LoadEnd)
            yield break;

        AssetBundle bundle = loadReqData.m_bundleLoadReq.m_assetBundle;
        //シーンのアセットバンドルはLoadAllAssetsAsyncで呼ぶとエラーとなるので回避する
        AssetBundleRequest tmpBundleReq = null;
        if (!bundle.isStreamedSceneAssetBundle)
        {
            tmpBundleReq = bundle.LoadAllAssetsAsync();
        }
        
        if(tmpBundleReq != null)
        {
            while (!tmpBundleReq.isDone)
            {
                yield return m_loadWaitTime;
            }
            loadReqData.m_assetDatas = tmpBundleReq.allAssets;
        }
        

        yield return GetAssetAsync(bundle, loadReqData);
    }


    /// <summary>
    /// 指定したアセットデータの取得処理
    /// </summary>
    /// <param name="bundle">AssetBundle</param>
    /// <param name="loadReqData">AssetBundleのロードデータ</param>
    /// <returns>アセットデータ</returns>
    private static IEnumerator GetAssetAsync(AssetBundle bundle, ABLoadReqData loadReqData)
    {
        if (loadReqData.m_assetName == "")
            yield break;

        AssetBundleRequest tmpBundleReq = bundle.LoadAssetAsync(loadReqData.m_assetName);

        while (!tmpBundleReq.isDone) {
            yield return m_loadWaitTime;
        }

        loadReqData.m_assetData = tmpBundleReq.asset;
    }


    /// <summary>
    /// 指定したアセットデータの取得処理
    /// </summary>
    /// <param name="bundle">AssetBundle</param>
    /// <param name="loadReqData">AssetBundleのロードデータ</param>
    /// <returns>アセットデータ</returns>
    private static void GetAsset(AssetBundle bundle, ABLoadReqData loadReqData)
    {
        if (loadReqData.m_assetName == "")
            return;

        loadReqData.m_assetData = bundle.LoadAsset(loadReqData.m_assetName);
    }


    /// <summary>
    /// アセットバンドルの消去処理
    /// </summary>
    /// <param name="bundleName">バンドル名</param>
    public static IEnumerator ClearAssetBundle(string bundleName)
    {
        ABLoadReqData tmpReq = GetABLoadReq(bundleName);

        if (tmpReq == null || !m_ABReqload.ContainsKey(bundleName) || tmpReq.m_bundleLoadReq.m_assetBundle == null)
        {
            Debug.LogWarning("Not Loaded Bundle ：" + bundleName);
            yield break;
        }

        tmpReq.m_bundleLoadReq.m_assetBundle.Unload(true);
        yield return null;

        m_ABReqload.Remove(bundleName);
    }
}
